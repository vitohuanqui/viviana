# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models import ImageField

from places.models import Place


class MainCategory(models.Model):

    name = models.CharField('name', max_length=250)
    image = ImageField('image', upload_to='images', null=True, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'main category'
        verbose_name_plural = 'main categories'


class Category(models.Model):

    main_category = models.ForeignKey(
        MainCategory, verbose_name='main category', null=True, blank=True)
    name = models.CharField('name', max_length=250)
    image = ImageField('image', upload_to='images', null=True, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'category'
        verbose_name_plural = 'categories'


class Product(models.Model):

    name = models.CharField('name', max_length=250)
    category = models.ForeignKey(Category)
    price = models.FloatField()
    description = models.TextField('Description', blank=True, null=True)
    place = models.ForeignKey(Place)
    image = ImageField('image', upload_to='images', null=True, blank=True)


class Offer(models.Model):

    name = models.CharField('name', max_length=250)
    price = models.FloatField()
    description = models.TextField('Description', blank=True, null=True)
    date_in = models.DateField()
    date_out = models.DateField()
    image = ImageField('image', upload_to='images', null=True, blank=True)
    place = models.ForeignKey(Place)
