# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from django.contrib import admin

from .models import *


@admin.register(MainCategory)
class UserProfileAdmin(admin.ModelAdmin):

    list_display = ('name', 'image')
    search_fields = ('user',)


@admin.register(Category)
class ClientAdmin(admin.ModelAdmin):

    list_display = ('main_category', 'name', 'image',)
    search_fields = ('name',)


@admin.register(Product)
class CompanyAdmin(admin.ModelAdmin):

    class Meta:
        model = Product
