from django.conf.urls import include, url

from rest_framework.routers import DefaultRouter
from rest_framework import renderers
from .views import *

places = ListPlaces.as_view({
    'get': 'list'
})
places_owner = ListPlaces.as_view({
    'get': 'list_owner',
})
search_places = GetProducts.as_view({
    'get': 'get_by_product',
})

urlpatterns = [
    url(r'^places/$', places, name='places-list'),
    url(r'^user/places/$', places_owner, name='user-places-list'),
    url(r'^user/places/$', places_owner, name='user-places-list'),
    url(r'^user/places/management/$', GetPlace.as_view()),
    url(r'^classes/Category$', GetCategories.as_view()),
    url(r'^classes/Search/(?P<string>\w+)', search_places),
]
