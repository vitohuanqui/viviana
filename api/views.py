# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.measure import D

from places.models import Place
from products.models import Category, Product
from .serializers import PlaceSerializer, UserProfile, CategorySerializer

from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework import authentication, permissions, status


class GetProducts(ViewSet):

    def get_by_product(self, request, string):
        print(string)
        places = Place.objects.filter(name__contains=string)
        products = Product.objects.filter(
            name__contains=string, category__name__contains=string,
            description__contains=string).values_list('place')
        serializer = PlaceSerializer(places, many=True)
        serializer_2 = PlaceSerializer(places, many=True)
        s = dict(serializer.data.items() + serializer_2.data.items())
        return Response(s)


class ListPlaces(ViewSet):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """


    def list(self, request):
        """
        Return a list of all places.
        """

        if request.user.is_authenticated():
            radius = request.user.radius
        else:
            radius = 10
        pnt = GEOSGeometry('POINT({lat} {lon})'.format(lat=2, lon=3), srid=4326)
        queryset = Place.objects.filter(mpoly__distance_lt=(pnt, D(km=radius)))
        serializer = PlaceSerializer(queryset, many=True)
        return Response(serializer.data)

    def list_owner(self, request):
        """
        Return a list of all places.
        """
        queryset = get_object_or_404(Place, user=request.user)
        serializer = PlaceSerializer(queryset, many=True)
        return Response(serializer.data)


class GetPlace(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """

    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAdminUser,)

    def get(self, request, place_id):
        """
        Return a list of all users.
        """
        place = get_object_or_404(Place, id=place_id)
        serializer = PlaceSerializer(place)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = PlaceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, place_id, format=None):
        place = get_object_or_404(Place, id=place_id)
        serializer = PlaceSerializer(place, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, place_id, format=None):
        place = get_object_or_404(Place, id=place_id)
        place.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class GetCategories(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    #
    # authentication_classes = (authentication.TokenAuthentication,)
    # permission_classes = (permissions.IsAdminUser,)

    def get(self, request):
        """
        Return a list of all users.
        """
        categories = Category.objects.all()
        serializer = CategorySerializer(categories,  many=True)
        return Response(serializer.data)

    def post(self, request, format=None):

        categories = Category.objects.all()
        serializer = CategorySerializer(categories,  many=True)
        return Response(serializer.data)
        serializer = PlaceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, place_id, format=None):
        place = get_object_or_404(Place, id=place_id)
        serializer = PlaceSerializer(place, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, place_id, format=None):
        place = get_object_or_404(Place, id=place_id)
        place.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)