from rest_framework import serializers

from places.models import Place
from products.models import Product, Offer, MainCategory, Category
from common.models import UserProfile

from drf_extra_fields.geo_fields import PointField


class PointFieldSerializer(serializers.Serializer):

    point = PointField(required=False)


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile


class PlaceSerializer(serializers.ModelSerializer):

    mpoly = PointFieldSerializer(many=False, read_only=True)
    user = UserSerializer(many=False, read_only=True)

    class Meta:
        model = Place
        fields = (
            'name', 'area', 'lon', 'lat', 'phone', 'address', 'description',
            'upvote_number', 'downvote_number', 'image', 'user', 'mpoly')


class MainCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = MainCategory


class CategorySerializer(serializers.ModelSerializer):
    title = serializers.CharField(source='name')
    icon = serializers.ImageField(source='image')
    imageThumb = serializers.ImageField(source='image')

    class Meta:
        model = Category
        fields = ('title', 'icon', 'image', 'imageThumb')
