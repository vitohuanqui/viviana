# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.gis.db import models

from places.models import Place
from mapwidgets.widgets import GooglePointFieldWidget


@admin.register(Place)
class CompanyAdmin(admin.ModelAdmin):

    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget}
    }

    class Meta:
        model = Place
