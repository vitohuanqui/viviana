# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.db.models import ImageField

from common.models import UserProfile


class Place(models.Model):

    name = models.CharField(max_length=100)
    area = models.IntegerField()
    lon = models.FloatField()
    lat = models.FloatField()
    phone = models.CharField('phone', max_length=50)
    address = models.CharField('address', max_length=250)
    description = models.TextField('Description', blank=True, null=True)
    upvote_number = models.IntegerField('upvote number', default=0)
    downvote_number = models.IntegerField('downvote number', default=0)
    image = ImageField('image', upload_to='images', null=True, blank=True)
    user = models.ForeignKey(UserProfile)
    # GeoDjango-specific: a geometry field (MultiPolygonField)
    mpoly = models.PointField()

    # Returns the string representation of the model.
    def __str__(self):              # __unicode__ on Python 2
        return self.name